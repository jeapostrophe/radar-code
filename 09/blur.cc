#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <sys/param.h>
#include <pthread.h>

typedef void (blur_fun)( int, double, double[], double[] );

double gaussian( double x, double s ) {
  double dv = x / s;
  return ((exp(-0.5 * dv * dv)) / (sqrt(2.0*M_PI) * s));
}

double *guassian1d(int mn, int mx, int ss_len, double sigma) {
  double *ss = (double *)malloc(sizeof(double)*ss_len);
  { double sum = 0.0;
    for ( int i = 0; i < ss_len; i++ ) {
      double v = gaussian( (i + mn), sigma );
      ss[i] = v;
      sum += v;
    }
    for ( int i = 0; i < ss_len; i++ ) {
      ss[i] = ss[i] / sum;
    } }
  return ss;
}

int C = 4;

void raw_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  { int i = 0;
    for ( int y = 0; y < N; y++ ) {
      for ( int x = 0; x < N; x++ ) {
        int dx_start = MAX(x + mn, 0) - x;
        int dx_end   = MIN(x + mx, N) - x;
        for ( int k = 0; k < C; k++ ) {
          int j = i + C * dx_start;
          double sum = 0.0;
          for ( int dx = dx_start; dx < dx_end; dx++ ) {
            sum += ss[dx - mn] * in[j];
            j += C;
          }
          out[(k + (C * (x + (y * N))))] = sum;
          i++;
        } } } }

  free(ss);

  return;
}

#include "immintrin.h"
void vec_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  { int i = 0;
    for ( int y = 0; y < N; y++ ) {
      for ( int x = 0; x < N; x++ ) {
        int dx_start = MAX(x + mn, 0) - x;
        int dx_end   = MIN(x + mx, N) - x;
        int j = i + C * dx_start;

        __m256d sumV = _mm256_set_pd(0.0, 0.0, 0.0, 0.0);
        for ( int dx = dx_start; dx < dx_end; dx++ ) {
          __m256d ssV = _mm256_broadcast_sd(ss + dx - mn);
          __m256d inV = _mm256_load_pd(in + j);
          __m256d ansV = _mm256_mul_pd(ssV, inV);
          sumV = _mm256_add_pd(sumV, ansV);
          j += C;
        }
        _mm256_store_pd (out + (C * (x + (y * N))), sumV);

        i += C;} } }

  free(ss);

  return;
}

void auto_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  for ( int y = 0; y < N; y++ ) {
    for ( int x = 0; x < N; x++ ) {
      int dx_start = MAX(x + mn, 0) - x;
      int dx_end   = MIN(x + mx, N) - x;
      #pragma vector aligned
      #pragma vector nontemporal
      for ( int k = 0; k < C; k++ ) {
        double sum = 0.0;
        for ( int dx = dx_start; dx < dx_end; dx++ ) {
          sum += ss[dx - mn] * in[(k + (C * (dx + (y * N))))];
        }
        out[(k + (C * (x + (y * N))))] = sum;
      } } }
  _mm_sfence();
  
  free(ss);

  return;
}

typedef struct {
  int N;
  double *in;
  double *out;
  int mn;
  int mx;
  double *ss;
  int y_start;
  int y_end;
} thr_blur_arg_t;

void *thr_blur_inner(void *varg) {
  thr_blur_arg_t *arg = (thr_blur_arg_t *)varg;

  for ( int y = arg->y_start; y < arg->y_end; y++ ) {
    for ( int x = 0; x < arg->N; x++ ) {
      int dx_start = MAX(x + arg->mn, 0) - x;
      int dx_end   = MIN(x + arg->mx, arg->N) - x;
      #pragma vector aligned
      #pragma vector nontemporal
      for ( int k = 0; k < C; k++ ) {
        double sum = 0.0;
        for ( int dx = dx_start; dx < dx_end; dx++ ) {
          sum += arg->ss[dx - arg->mn] * arg->in[(k + (C * (dx + (y * arg->N))))];
        }
        arg->out[(k + (C * (x + (y * arg->N))))] = sum;
      } } }
  
  _mm_sfence();

  return NULL;
}

void thr_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  pthread_t side1_t, side2_t;
  thr_blur_arg_t side1 = { N, in, out, mn, mx, ss,   0, N/2 };
  thr_blur_arg_t side2 = { N, in, out, mn, mx, ss, N/2,   N };

  pthread_create(&side1_t, NULL, thr_blur_inner, &side1);
  pthread_create(&side2_t, NULL, thr_blur_inner, &side2);

  pthread_join(side1_t, NULL);
  pthread_join(side2_t, NULL);

  free(ss);

  return;
}

#ifndef __INTEL_COMPILER
#include "tbb/tbb.h"
using namespace tbb;
void tbb_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  parallel_for(
   blocked_range<size_t>(0,N,N/2),
   [=](const blocked_range<size_t>& r) {
     for(size_t y=r.begin(); y!=r.end(); ++y) {
       for ( int x = 0; x < N; x++ ) {
         int dx_start = MAX(x + mn, 0) - x;
         int dx_end   = MIN(x + mx, N) - x;
         #pragma vector aligned
         #pragma vector nontemporal
         for ( int k = 0; k < C; k++ ) {
           double sum = 0.0;
           for ( int dx = dx_start; dx < dx_end; dx++ ) {
             sum += ss[dx - mn] * in[(k + (C * (dx + (y * N))))];
           }
           out[(k + (C * (x + (y * N))))] = sum;
         } } } } );
  _mm_sfence();
  
  free(ss);

  return;
}
#endif

int main(int argc, char **argv) {
  if (argc != 6) {
    fprintf(stderr, "usage: blur <mode> <N> <sigma> <in-file> <out-file>\n");
    exit(1);
  }

  blur_fun *bf = NULL;
  if ( strcmp(argv[1],   "raw") == 0 ) { bf = raw_blur; }
  if ( strcmp(argv[1],   "vec") == 0 ) { bf = vec_blur; }
  if ( strcmp(argv[1],  "auto") == 0 ) { bf = auto_blur; }
  if ( strcmp(argv[1],   "thr") == 0 ) { bf = thr_blur; }
#ifndef __INTEL_COMPILER
  if ( strcmp(argv[1],   "tbb") == 0 ) { bf = tbb_blur; }
#endif
  
  if ( bf == NULL ) {
    fprintf(stderr, "unknown mode: %s\n", argv[1] );
    exit(1);
  }

  int N = atoi(argv[2]);
  double sigma = atof(argv[3]);
  char *inf = argv[4];
  char *outf = argv[5];

  int len = sizeof(double)*N*N*C;

  int infd = open(inf, O_RDWR);
  if (infd == -1) { perror("open input"); exit(1); }

  int outfd = open(outf, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
  if (outfd == -1) { perror("open output"); exit(1); }
  if (lseek(outfd, len-1, SEEK_SET) == -1) { perror("open lseek"); exit(1); }
  if (write(outfd, "", 1) == -1) { perror("open write"); exit(1); }

  double *in = (double *)mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_PRIVATE, infd, 0);
  if ( in == MAP_FAILED ) { perror("mmap input"); exit(1); }

  double *out = (double *)mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_SHARED, outfd, 0);
  if ( out == MAP_FAILED ) { perror("mmap output"); exit(1); }

  bf( N, sigma, in, out );

  msync(out, len, MS_SYNC);
  munmap(out, len);
  munmap(in, len);
  close(infd);
  close(outfd);

  return 0;
}
