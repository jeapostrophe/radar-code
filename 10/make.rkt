#!/usr/bin/env racket
#lang racket/base
(require racket/system
         racket/format)

(define (sys . a)
  (define cmd (apply ~a a))
  (eprintf "$ ~a\n" cmd)
  (unless (system cmd)
    (error 'sys "Command failed!")))

(module+ main
  (require racket/cmdline)
  (define root "example")

  (define test? #f)
  (command-line #:program "make"
                #:once-any
                [("--test")
                 "Turn on test mode (runs faster)"
                 (set! test? #t)])

  (define-values (Sigmas Ts modes)
    (values '(16)
            (if test?
              '(7)
              '(7 8 9 10 11 12))
            '(raw vec auto thr tbb mpi)))

  (sys "/usr/bin/env I_MPI_CFLAGS=\"-DBLUR_MPI -std=c++11 -Wall -O3 -fstrict-aliasing -m64 -march=native\" I_MPI_LDFLAGS=\"-lm -ltbb -lstdc++\" ~/local/intel/impi/2018.1.163/bin64/mpigcc blur.cc -o ./blur.cc.mpi.bin")
  (sys "gcc -std=c++11 -Wall -O3 -fstrict-aliasing -m64 -march=native blur.cc -lm -lpthread -ltbb -lstdc++ -o blur.cc.bin")

  (for ([sigma (in-list Sigmas)])
    (for ([T (in-list Ts)])
      (define N (expt 2 T))
      (define (fm x y)
        (format "~a-~a-~a" sigma N (format x y)))
      (define in.bin (fm "~a.in.bin" root))
      (define in.png (fm "~a.in.png" root))
      (define out.bin (fm "~a.out.bin" root))
      (define blur.png (fm "~a.blur.png" root))
      (define out.png (fm "~a.out.png" root))
      (sys "./tool.rkt -N " N " --render " sigma " " in.bin " " in.png " " blur.png)

      (for ([mode (in-list modes)])
        (define before (current-inexact-milliseconds))
        (sys "rm -f /tmp/out.bin")
        (if (eq? mode 'mpi)
          (sys "~/local/intel/impi/2018.1.163/bin64/mpirun ./blur.cc.mpi.bin " mode " " N " " sigma " " in.bin " /tmp/out.bin")
          (sys "./blur.cc.bin " mode " " N " " sigma " " in.bin " /tmp/out.bin"))
        (define after (current-inexact-milliseconds))
        (printf "#(~a ~a ~a ~a)\n" sigma N mode (- after before))
        (flush-output))

      (sys "rm -f " in.bin " " in.png " " blur.png))))
