#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <sys/param.h>

typedef void (blur_fun)( int, double, double[], double[] );

double gaussian( double x, double s ) {
  double dv = x / s;
  return ((exp(-0.5 * dv * dv)) / (sqrt(2.0*M_PI) * s));
}

double *guassian1d(int mn, int mx, int ss_len, double sigma) {
  double *ss = malloc(sizeof(double)*ss_len);
  { double sum = 0.0;
    for ( int i = 0; i < ss_len; i++ ) {
      double v = gaussian( (i + mn), sigma );
      ss[i] = v;
      sum += v;
    }
    for ( int i = 0; i < ss_len; i++ ) {
      ss[i] = ss[i] / sum;
    } }
  return ss;
}

int C = 4;

void raw_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  { int i = 0;
    for ( int y = 0; y < N; y++ ) {
      for ( int x = 0; x < N; x++ ) {
        int dx_start = MAX(x + mn, 0) - x;
        int dx_end   = MIN(x + mx, N) - x;
        for ( int k = 0; k < C; k++ ) {
          int j = i + C * dx_start;
          double sum = 0.0;
          for ( int dx = dx_start; dx < dx_end; dx++ ) {
            sum += ss[dx - mn] * in[j];
            j += C;
          }
          out[(k + (C * (x + (y * N))))] = sum;
          i++;
        } } } }

  free(ss);

  return;
}

#ifdef NEVER_DEFINE_THIS_CODE_IS_BROKEN
void example() {
  // Before
  for ( int k = 0; k < C; k++ ) {
    int j = i + C * dx_start;
    double sum = 0.0;
    for ( int dx = dx_start; dx < dx_end; dx++ ) {
      sum += ss[dx - mn] * in[j];
      j += C;
    }
    out[(k + (C * (x + (y * N))))] = sum;
    i++;
  }
  // Unroll this loop  
  {
    int j = i + C * dx_start;
    double sum0 = 0.0;
    double sum1 = 0.0;
    double sum2 = 0.0;
    double sum3 = 0.0;
    for ( int dx = dx_start; dx < dx_end; dx++ ) {
      double theSS = ss[dx - mn];
      sum0 += theSS * in[j + 0];
      sum1 += theSS * in[j + 1];
      sum2 += theSS * in[j + 2];
      sum3 += theSS * in[j + 3];
      j += C;
    }
    out[(0 + (C * (x + (y * N))))] = sum0;
    out[(1 + (C * (x + (y * N))))] = sum1;
    out[(2 + (C * (x + (y * N))))] = sum2;
    out[(3 + (C * (x + (y * N))))] = sum3;
    i += 4;
  }
}
#endif

#include "immintrin.h"
void vec_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  { int i = 0;
    for ( int y = 0; y < N; y++ ) {
      for ( int x = 0; x < N; x++ ) {
        int dx_start = MAX(x + mn, 0) - x;
        int dx_end   = MIN(x + mx, N) - x;
        int j = i + C * dx_start;

        __m256d sumV = _mm256_set_pd(0.0, 0.0, 0.0, 0.0);
        for ( int dx = dx_start; dx < dx_end; dx++ ) {
          __m256d ssV = _mm256_broadcast_sd(ss + dx - mn);
          __m256d inV = _mm256_load_pd(in + j);
          __m256d ansV = _mm256_mul_pd(ssV, inV);
          sumV = _mm256_add_pd(sumV, ansV);
          j += C;
        }
        _mm256_store_pd (out + (C * (x + (y * N))), sumV);
        
        i += C;} } }

  free(ss);

  return;
}

#ifdef __INTEL_COMPILER
#include <mkl.h>
void mkl_blur( int N, double sigma, double in[], double out[] ) {
  int mn = floor(-3.0 * sigma);
  int mx = 1 + ceil(3.0 * sigma);
  int ss_len = mx - mn;
  double *ss = guassian1d(mn, mx, ss_len, sigma);

  // Broadcast four-wide
  double *quad_ss = malloc(sizeof(double)*C*ss_len);
  for ( int i = 0; i < ss_len; i++ ) {
    for ( int k = 0; k < C; k++ ) {
      quad_ss[i*C + k] = ss[i];
    }
  }

  { double *quad_sumsV = malloc(sizeof(double)*ss_len*C);
    
    int i = 0;
    for ( int y = 0; y < N; y++ ) {
      for ( int x = 0; x < N; x++ ) {
        int dx_start = MAX(x + mn, 0) - x;
        int dx_end   = MIN(x + mx, N) - x;
        int len = dx_end - dx_start;

        int j = i + C * dx_start;
        double *quad_ssV = quad_ss + C*(dx_start - mn);

        vdMul( 4 * len, quad_ssV, in + j, quad_sumsV );
        double sum0 = cblas_dasum(len, quad_sumsV + 0, C);
        double sum1 = cblas_dasum(len, quad_sumsV + 1, C);
        double sum2 = cblas_dasum(len, quad_sumsV + 2, C);
        double sum3 = cblas_dasum(len, quad_sumsV + 3, C);

        out[(0 + (C * (x + (y * N))))] = sum0;
        out[(1 + (C * (x + (y * N))))] = sum1;
        out[(2 + (C * (x + (y * N))))] = sum2;
        out[(3 + (C * (x + (y * N))))] = sum3;

        i += C;
      } }

    free(quad_sumsV); }

  free(quad_ss);
  free(ss);

  return;
}
#endif

int main(int argc, char **argv) {
  if (argc != 6) {
    fprintf(stderr, "usage: blur <mode> <N> <sigma> <in-file> <out-file>\n");
    exit(1);
  }

  blur_fun *bf = NULL;
  if ( strcmp(argv[1], "raw") == 0 ) { bf = raw_blur; }
  if ( strcmp(argv[1], "vec") == 0 ) { bf = vec_blur; }
#ifdef __INTEL_COMPILER
  if ( strcmp(argv[1], "mkl") == 0 ) { bf = mkl_blur; }
#endif

  if ( bf == NULL ) {
    fprintf(stderr, "unknown mode: %s\n", argv[1] );
    exit(1);
  }

  int N = atoi(argv[2]);
  double sigma = atof(argv[3]);
  char *inf = argv[4];
  char *outf = argv[5];

  int len = sizeof(double)*N*N*C;

  int infd = open(inf, O_RDWR);
  if (infd == -1) { perror("open input"); exit(1); }

  int outfd = open(outf, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
  if (outfd == -1) { perror("open output"); exit(1); }
  if (lseek(outfd, len-1, SEEK_SET) == -1) { perror("open lseek"); exit(1); }
  if (write(outfd, "", 1) == -1) { perror("open write"); exit(1); }

  double *in = (double *)mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_PRIVATE, infd, 0);
  if ( in == MAP_FAILED ) { perror("mmap input"); exit(1); }

  double *out = (double *)mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_SHARED, outfd, 0);
  if ( out == MAP_FAILED ) { perror("mmap output"); exit(1); }

  bf( N, sigma, in, out );

  msync(out, len, MS_SYNC);
  munmap(out, len);
  munmap(in, len);
  close(infd);
  close(outfd);

  return 0;
}
