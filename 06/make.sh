#!/bin/bash
source ~/intel/bin/iccvars.sh intel64
source ~/intel/mkl/bin/mklvars.sh intel64
racket make.rkt | tee stats.rktd

# 3-gcc-raw 3-clang-raw 3-icc-raw 3-gcc-vec 3-clang-vec 3-icc-vec 3-icc-mkl 

# 0-gcc-raw 0-clang-raw 0-icc-raw 2-gcc-raw 2-clang-raw 2-icc-raw 3-gcc-raw 3-clang-raw 3-icc-raw

# 0-gcc-raw 0-clang-raw 0-icc-raw 1-gcc-raw 1-clang-raw 1-icc-raw 0-gcc-vec 0-clang-vec 0-icc-vec 1-gcc-vec 1-clang-vec 1-icc-vec
