#include <iostream>
#include <chrono>
#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <optional>
#include <random>
#include <condition_variable>

static const int M = 1024;

using namespace std;

// Semaphores
class semaphore {
private:
  mutex mutex_;
  condition_variable condition_;
  unsigned long count_ = 0; // Initialized as locked.

public:
  void notify() {
    unique_lock<decltype(mutex_)> lock(mutex_);
    ++count_;
    condition_.notify_one(); }

  void wait() {
    unique_lock<decltype(mutex_)> lock(mutex_);
    while(!count_) // Handle spurious wake-ups.
      condition_.wait(lock);
    --count_; }

  bool try_wait() {
    unique_lock<decltype(mutex_)> lock(mutex_);
    if(count_) {
      --count_;
      return true; }
    return false; } };

// General work specification

// Mul4
#if 0
typedef double work_t;
work_t make_work() {
  return drand48(); }
work_t do_work(work_t v) {
  return v * 4.0; }
#endif

// Fibonacci
typedef int work_t;
work_t make_work() {
  return 10 + (rand() % 20); }
work_t do_work(work_t v) {
  if ( v <= 1 ) {
    return 1;
  } else {
    return do_work( v - 1 ) + do_work( v - 2 ); } }

// Coordination Structures
typedef struct {
  work_t v;
} job_t;
typedef vector<job_t>(contend_fun)( const int );

// Single In Order
vector<job_t> single_inorder( const int N ) {
  vector<job_t> in(N * M);

  for ( auto j : in ) {
    j.v = make_work();
    j.v = do_work(j.v);
  }

  return in; }

// Single Produce, then Consume
vector<job_t> single_pro_con( const int N ) {
  vector<job_t> in(N * M);

  for ( int i = 0; i < N*M; i++ ) {
    in[i].v = make_work(); }

  for ( int i = 0; i < N*M; i++ ) {
    in[i].v = do_work(in[i].v); }

  return in; }

// Split into N threads and In Order
vector<job_t> split_inorder( const int N ) {
  vector<job_t> in(N * M);

  auto split_inorder1 =
    [=]( int rank ) {
    int start = (rank + 0) * M;
    int   end = (rank + 1) * M;

    for ( int i = start; i < end; i++ ) {
      job_t j = in[i];
      j.v = make_work();
      j.v = do_work(j.v); } };

  vector<thread> threads(N);
  for ( int i = 0; i < N; i++ ){
    threads[i] = thread(split_inorder1, i); }
  for ( int i = 0; i < N; i++ ){
    threads[i].join(); }

  return in; }

// Split into N threads and Produce then Consume
vector<job_t> split_pro_con( const int N ) {
  vector<job_t> in(N * M);

  auto split_pro_con1 =
    [&]( int rank ) {
    int start = (rank + 0) * M;
    int   end = (rank + 1) * M;

    for ( int i = start; i < end; i++ ) {
      in[i].v = make_work(); }

    for ( int i = start; i < end; i++ ) {
      job_t j = in[i];
      j.v = do_work(j.v); } };

  vector<thread> threads(N);
  for ( int i = 0; i < N; i++ ){
    threads[i] = thread(split_pro_con1, i); }
  for ( int i = 0; i < N; i++ ){
    threads[i].join(); }

  return in; }

// Producer-Consumer with Lock
vector<job_t> proco_lock( const int N ) {
  vector<job_t> in(N * M);
  queue<int> job_queue;
  mutex mut;
  condition_variable cv;

  auto consumer =
    [&]() {
    int task = 0;
    while ( task != -1 ) {
      { unique_lock<mutex> lock(mut);
        cv.wait(lock, [&]{return ! job_queue.empty();});
        task = job_queue.front();
        if ( task != -1 ) {
          job_queue.pop(); } }
      if ( task != -1 ) {
        job_t j = in[task];
        j.v = do_work(j.v); } } };

  vector<thread> consumers(N);
  for ( int i = 0; i < N; i++ ){
    consumers[i] = thread(consumer); }

  // Do Producing
  for ( int i = 0; i < N*M; i++ ) {
    in[i].v = make_work();
    { lock_guard<mutex> lock(mut);
      job_queue.push(i); }
    cv.notify_one(); }
  { lock_guard<mutex> lock{mut};
    job_queue.push(-1); }
  cv.notify_all();

  for ( int i = 0; i < N; i++ ){
    consumers[i].join(); }

  return in; }

// Lock-Free Producer-Consumer
vector<job_t> proco_lf( const int N ) {
  vector<job_t> in(N * M);
  vector<optional<int>> slots(N);
  vector<semaphore> sems(N);

  auto consumer =
    [&](int me) {
    int task = 0;
    while ( task != -1 ) {
      while ( ! slots[me] ) {
        sems[me].wait(); }
      task = slots[me].value();
      if ( task != -1 ) {
        slots[me].reset();
        job_t j = in[task];
        j.v = do_work(j.v); } } };

  vector<thread> consumers(N);
  for ( int i = 0; i < N; i++ ) {
    consumers[i] = thread(consumer, i); }

  int curr_slot = 0;
  for ( int i = 0; i < N*M; i++ ) {
    in[i].v = make_work();
    while ( slots[curr_slot] ) {
      curr_slot = (curr_slot + 1) % N; }
    slots[curr_slot] = i;
    sems[curr_slot].notify(); }
  for ( int i = 0; i < N; i++ ) {
    while ( slots[curr_slot] ) {
      curr_slot = (curr_slot + 1) % N; }
    slots[curr_slot] = -1;
    sems[curr_slot].notify(); }

  for ( int i = 0; i < N; i++ ){
    consumers[i].join(); }

  return in; }

// Main
int main(int argc, char **argv) {
  if (argc != 3) {
    cerr << "usage: contend <mode> <N>\n";
    exit(1); }

  contend_fun *cf = NULL;
#define CF_SELECT(ts, tf) if ( string(argv[1]) == ts ) { cf = tf; }
  CF_SELECT("single_inorder", single_inorder);
  CF_SELECT("single_pro_con", single_pro_con);
  CF_SELECT("split_inorder", split_inorder);
  CF_SELECT("split_pro_con", split_pro_con);
  CF_SELECT("proco_lock", proco_lock);
  CF_SELECT("proco_lf", proco_lf);
  if ( cf == NULL ) {
    cerr << "unknown mode: " << argv[1] << "\n";
    exit(1); }

  int N = atoi(argv[2]);

  auto start = chrono::steady_clock::now();
  auto res = cf(N);
  chrono::nanoseconds total_time = (chrono::steady_clock::now() - start);

  cout << "#(" << argv[1]
       << " " << N
       << " " << M
       << " " << total_time.count()
       << ")\n";

  return 0; }
