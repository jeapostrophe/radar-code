#define MODE "08-sliding"
#include "main.cc"

void go(uint N, vector<float> C, vector<float> T) {
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    float one = C[j + G + N_cfar];
    float two = C[j - G];
    float thr = C[j - G - N_cfar];
    float fou = C[j + G];
    T[j] = (0.5 * (1.0/N_cfar) *
            ((one*one + two*two) - (thr*thr + fou*fou)); }

  float sum = 0.0;
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j ++ ) {
    sum += T[j];
    T[j] = sum; } }
