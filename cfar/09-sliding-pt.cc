#define MODE "09-sliding-pt"
#include "main.cc"
#include <thread>
#define CPUs 2

void go(uint N, vector<float> C, vector<float> T) {
  vector<thread> threads(CPUs);

  for ( uint i = 0; i < CPUs; i++ ) {
    threads[i] =
      thread([&](){
          #pragma omp simd
          for ( uint j = (G + N_cfar) + i*N/CPUs;
                j < (i+1)*N/CPUs + (G + N_cfar);
                j++ ) {
    float one = C[j + G + N_cfar];
    float two = C[j - G];
    float thr = C[j - G - N_cfar];
    float fou = C[j + G];
    T[j] = (0.5 * (1.0/N_cfar) * one*one + two*two)
    - (thr*thr + fou*fou); } }); }

  for ( uint i = 0; i < CPUs; i++ ) {
    threads[i].join(); }
  
  float sum = 0.0;
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j ++ ) {
    sum += T[j];
    T[j] = sum; } }
