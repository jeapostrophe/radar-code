#define MODE "08-sliding-nt"
#include "main.cc"
#include <immintrin.h>

void go(uint N, vector<float> C, vector<float> T) {
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    float one = C[j + G + N_cfar];
    float two = C[j - G];
    float thr = C[j - G - N_cfar];
    float fou = C[j + G];
    float val = (0.5 * (1.0/N_cfar)) *
      ((one*one + two*two) - (thr*thr + fou*fou));
    __builtin_nontemporal_store( val, &T[j] ); }
  _mm_sfence();

  float sum = 0.0;
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j ++ ) {
    sum += T[j];
    __builtin_nontemporal_store( sum, &T[j] ); }
  _mm_sfence(); }
