#!/usr/bin/env racket
#lang racket/base
(require racket/list
         racket/file
         racket/format
         racket/match
         plot)

(define (graph! name vs)
  (define modes
    (sort
     (remove-duplicates
      (for/list ([v (in-list vs)]) (vector-ref v 0)))
     string<=?
     #:key symbol->string))
  (define (select k)
    (for/list ([v (in-list vs)]
               #:when (eq? (vector-ref v 0) k))
      (match-define (vector _ x t) v)
      (vector x (/ t (expt 10 4)))))
  (define N (length modes))
  (parameterize ([plot-y-transform log-transform])
    (plot (for/list ([m (in-list modes)]
                     [i (in-naturals)])
            (discrete-histogram (select m)
                                #:skip (+ N 0.5) #:x-min i
                                #:label (symbol->string m)
                                #:color i #:line-color i))
          #:y-min 1
          #:y-label "time (ms)"
          #:x-label "N"
          #:title "Plot")))

(module+ main
  (require racket/cmdline)
  (command-line
   #:program "graph"
   #:args stats
   (plot-new-window? #t)
   (graph! stats (append-map file->list stats))))
