#define MODE "06-sliding-simd"
#include "main.cc"
#include <immintrin.h>

void go(uint N, vector<float> C, vector<float> T) {
  float scale = 0.5 * (1.0/N_cfar);
  __m256 scalev = _mm256_broadcast_ss( &scale );
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j += 8 ) {
    __m256 one = _mm256_load_ps( &C[j + G + N_cfar] );
    one = _mm256_mul_ps( one, one );
    one = _mm256_mul_ps( one, scalev );
    __m256 two = _mm256_load_ps( &C[j - G] );
    __m256 left = _mm256_fmadd_ps( two, two, one );

    __m256 thr = _mm256_load_ps( &C[j - G - N_cfar] );
    thr = _mm256_mul_ps( thr, thr );
    __m256 fou = _mm256_load_ps( &C[j + G] );
    __m256 right = _mm256_fmadd_ps( fou, fou, thr );

    __m256 sumv = _mm256_sub_ps( left, right );
    _mm256_store_ps( &T[j], sumv ); }

  float sum = 0.0;
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j ++ ) {
    sum += T[j];
    T[j] = sum; } }
