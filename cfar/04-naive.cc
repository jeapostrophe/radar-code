#define MODE "04-naive"
#include "main.cc"

void go(uint N, vector<float> C, vector<float> T) {
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    float sum = 0.0;
    for ( uint l = G; l < G + N_cfar; l++ ) {
      sum += read(N, C, j+l) + read(N, C, j-l);  }
    T[j] = 0.5 * (1.0/N_cfar) * sum; } }
