#define MODE "09-sliding-tbb"
#include "main.cc"
#include <tbb/tbb.h>

using namespace tbb;
void go(uint N, vector<float> C, vector<float> T) {
  parallel_for(
   blocked_range<size_t>((G + N_cfar), N + (G + N_cfar), N/2),
   [&](const blocked_range<size_t>& r) {
     #pragma omp simd
     for(size_t j = r.begin(); j <= r.end(); ++j) {
       float one = C[j + G + N_cfar];
       float two = C[j - G];
       float thr = C[j - G - N_cfar];
       float fou = C[j + G];
       T[j] = (0.5 * (1.0/N_cfar) * one*one + two*two)
         - (thr*thr + fou*fou); } });
  
  float sum = 0.0;
  #pragma omp simd
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j ++ ) {
    sum += T[j];
    T[j] = sum; } }
