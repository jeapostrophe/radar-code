#define MODE "06-naive-mkl"
#include "main.cc"
#include <cblas.h>

void go(uint N, vector<float> C, vector<float> T) {
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    float *leftv = &C[j+G];
    float *rightv = &C[j-(G+N_cfar)];
    // XXX need to square leftv and rightv

    cblas_saxpy( N_cfar, 1.0, leftv, 1, rightv, 1 );
    float sum = cblas_sasum( N_cfar, rightv, 1 ); 

    T[j] = 0.5 * (1.0/N_cfar) * sum; } }
