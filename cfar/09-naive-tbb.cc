#define MODE "09-naive-tbb"
#include "main.cc"
#include <tbb/tbb.h>

using namespace tbb;

void go(uint N, vector<float> C, vector<float> T) {
  parallel_for(
   blocked_range<size_t>((G + N_cfar), N + (G + N_cfar), N/2),
   [&](const blocked_range<size_t>& r) {
     for(size_t j=r.begin(); j!=r.end(); ++j) {
       float sum = 0.0;
       #pragma omp simd
       for ( uint l = G; l < G + N_cfar; l++ ) {
         float left = C[j+l];
         float right = C[j-l];
         sum += left*left + right*right;  }
       T[j] = 0.5 * (1.0/N_cfar) * sum; } } ); }
