#include <iostream>
#include <chrono>
#include <random>
#include <vector>
#include <cmath>

using namespace std;

uint G = 16;
uint N_cfar = 24;

inline float read(uint N, vector<float> in, uint j) {
  return pow(abs(in[j]), 2); }

void go(uint N, vector<float> in, vector<float> out);

void prep(uint N, vector<float> in) {
  default_random_engine generator;
  uniform_real_distribution<float> distribution(0.0,1.0);
  
  for ( uint i = (G + N_cfar); i < N; i++ ) {
    in[i] = distribution(generator); } }

int main(int argc, char **argv) {
  if (argc != 2) {
    cerr << "usage: " << MODE << " <N>\n";
    exit(1); }

  uint N = atoi(argv[1]);

  vector<float> in(N + 2 * (G + N_cfar), 0.0);
  prep(N, in);
  vector<float> out(N + 2 * (G + N_cfar), 0.0);

  auto start = chrono::steady_clock::now();
  go(N, in, out);
  chrono::nanoseconds total_time = (chrono::steady_clock::now() - start);

  cout << "#(" << MODE
       << " " << N
       << " " << total_time.count()
       << ")\n"; }
