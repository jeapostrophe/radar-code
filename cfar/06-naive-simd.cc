#define MODE "06-naive-simd"
#include "main.cc"
#include <immintrin.h>

void go(uint N, vector<float> C, vector<float> T) {
  float zero = 0.0;
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    __m256 sumv = _mm256_broadcast_ss( &zero );
    for ( uint l = G; l < G + N_cfar; l += 8 ) {
      __m256 left  = _mm256_load_ps(&C[j-l]);
      __m256 right = _mm256_load_ps(&C[j+l]);
      left = _mm256_mul_ps( left, left );
      right = _mm256_mul_ps( right, right );
      sumv = _mm256_add_ps( sumv, _mm256_add_ps( left, right ) ); }
    sumv = _mm256_hadd_ps( sumv, sumv );
    sumv = _mm256_hadd_ps( sumv, sumv );
    sumv = _mm256_hadd_ps( sumv, sumv );
    __m128 sumsv = _mm256_extractf128_ps (sumv, 0);
    float sum;
    _MM_EXTRACT_FLOAT(sum, sumsv, 0);
    T[j] = 0.5 * (1.0/N_cfar) * sum; } }
