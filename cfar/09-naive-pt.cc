#define MODE "09-naive-pt"
#include "main.cc"
#include <thread>
#define CPUs 2

void go(uint N, vector<float> C, vector<float> T) {
  vector<thread> threads(CPUs);

  for ( uint i = 0; i < CPUs; i++ ) {
    threads[i] =
      thread([&](){
          for ( uint j = (G + N_cfar) + i*N/CPUs;
                j < (i+1)*N/CPUs + (G + N_cfar);
                j++ ) {
            float sum = 0.0;
            #pragma omp simd
            for ( uint l = G; l < G + N_cfar; l++ ) {
              float left = C[j+l];
              float right = C[j-l];
              sum += left*left + right*right;  }
            T[j] = 0.5 * (1.0/N_cfar) * sum; } }); }

  for ( uint i = 0; i < CPUs; i++ ) {
    threads[i].join(); } }
