#define MODE "08-naive"
#include "main.cc"

void go(uint N, vector<float> C, vector<float> T) {
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    float sum = 0.0;
    #pragma omp simd
    for ( uint l = G; l < G + N_cfar; l++ ) {
      float left = C[j+l];
      float right = C[j-l];
      sum += left*left + right*right;  }
    T[j] = 0.5 * (1.0/N_cfar) * sum; } }
