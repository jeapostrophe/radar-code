#define MODE "04-sliding"
#include "main.cc"

void go(uint N, vector<float> C, vector<float> T) {
  float last = 0.0;
  for ( uint j = (G + N_cfar); j < N + (G + N_cfar); j++ ) {
    last = last
      + 0.5 * (1.0/N_cfar) * read(N, C, j + G + N_cfar)
      + read(N, C, j - G)
      - read(N, C, j - G - N_cfar)
      - read(N, C, j + G );

    T[j] = last; } }
