#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <mkl.h>

// C += A x B
void naive_dgemm( const int N, double *A, double *B, double *C ) {
  for ( int i = 0; i < N; i++ ) {
    for ( int j = 0; j < N; j++ ) {
      for ( int k = 0; k < N; k++ ) {
        C[i*N + j] += A[i*N + k] * B[k*N + j];
      }
    }
  }
}

void stride1_dgemm( const int N, double *A, double *B, double *C ) {
  for ( int k = 0; k < N; k++ ) {
    for ( int i = 0; i < N; i++ ) {
      double r = A[i*N + k];
      for ( int j = 0; j < N; j++ ) {
        C[i*N + j] += r*B[k*N + j];
      }
    }
  }
}

void blocking_dgemm( int N, int M,
                     double *A, int ars, int are, int acs, int ace,
                     double *B, int brs, int bre, int bcs, int bce,
                     double *C, int crs, int cre, int ccs, int cce ) {
  if ( M <= 256 ) {
    for ( int ac = acs, br = brs; ac < ace && br < bre; ac++, br++ ) {
      for ( int cr = crs, ar = ars; cr < cre && ar < are; cr++, ar++ ) {
        double r = A[ar*N + ac];
        for ( int cc = ccs, bc = bcs; cc < cce && bc < bce; cc++, bc++ ) {
          C[cr*N + cc] += r*B[br*N + bc];
        }
      }
    }
  } else {
    int Mp = M/2;
    // C11 = A11 * B11
    blocking_dgemm( N, Mp,
                    A, ars, ars + Mp, acs, acs + Mp, // A11
                    B, brs, brs + Mp, bcs, bcs + Mp, // B11
                    C, crs, crs + Mp, ccs, ccs + Mp  // C11
                    );
    // C11 += A12 * B21
    blocking_dgemm( N, Mp,
                    A, ars, ars + Mp, acs + Mp, ace, // A12
                    B, brs + Mp, bre, bcs, bcs + Mp, // B21
                    C, crs, crs + Mp, ccs, ccs + Mp  // C11
                    );
    // C12 = A11 * B12
    blocking_dgemm( N, Mp,
                    A, ars, ars + Mp, acs, acs + Mp, // A11
                    B, brs, brs + Mp, bcs + Mp, bce, // B12
                    C, crs, crs + Mp, ccs + Mp, cce  // C12
                    );
    // C12 += A12 * B22
    blocking_dgemm( N, Mp,
                    A, ars, ars + Mp, acs + Mp, ace, // A12
                    B, brs + Mp, bre, bcs + Mp, bce, // B22
                    C, crs, crs + Mp, ccs + Mp, cce  // C12
                    );
    // C21 = A21 * B11
    blocking_dgemm( N, Mp,
                    A, ars + Mp, are, acs, acs + Mp, // A21
                    B, brs, brs + Mp, bcs, bcs + Mp, // B11
                    C, crs + Mp, cre, ccs, ccs + Mp  // C21
                    );
    // C21 += A22 * B21
    blocking_dgemm( N, Mp,
                    A, ars + Mp, are, acs + Mp, ace, // A22
                    B, brs + Mp, bre, bcs, bcs + Mp, // B21
                    C, crs + Mp, cre, ccs, ccs + Mp  // C21
                    );
    // C22 = A21 * B12
    blocking_dgemm( N, Mp,
                    A, ars + Mp, are, acs, acs + Mp, // A21
                    B, brs, brs + Mp, bcs + Mp, bce, // B12
                    C, crs + Mp, cre, ccs + Mp, cce  // C22
                    );
    // C22 += A22 * B22
    blocking_dgemm( N, Mp,
                    A, ars + Mp, are, acs + Mp, ace, // A22
                    B, brs + Mp, bre, bcs + Mp, bce, // B22
                    C, crs + Mp, cre, ccs + Mp, cce  // C22
                    );    
  }
}

double *random_matrix( int N, char rand ) {
  double *M = (double *)malloc(N * N * sizeof(double));
  if ( M == NULL ) {
    fprintf(stderr, "Failed to allocate matrix of size %dx%d\n", N, N);
    exit(1);
  }
  for ( int i = 0; i < N; i++ ) {
    for ( int j = 0; j < N; j++ ) {
      M[i * N + j] = rand ? drand48() * 8.0 : 0.0;
    }
  }
  return M;
}

int main(const int argc, const char **argv) {
  if ( argc != 3 ) {
    fprintf(stderr, "<gemm> <N> <mode>\n");
    exit(1);
  }

  int N = atoi(argv[1]);

  double *A = random_matrix(N, 1);
  double *B = random_matrix(N, 1);
  double *C = random_matrix(N, 0);

  const char *modes = argv[2];
  if ( strcmp(modes, "naive") == 0 ) {
    naive_dgemm( N, A, B, C );
  } else if ( strcmp(modes, "stride-1") == 0 ) {
    stride1_dgemm( N, A, B, C );
  } else if ( strcmp(modes, "blocking") == 0 ) {
    blocking_dgemm( N, N,
                    A, 0, N, 0, N,
                    B, 0, N, 0, N,
                    C, 0, N, 0, N );
  } else if ( strcmp(modes, "cblas") == 0 ) {
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                N, N, N, 1.0, A, N, B, N, 1.0, C, N);
  } else {
    fprintf(stderr, "Unknown mode: '%s'.\n", modes);
    exit(1);
  }

  return 0;
}
